﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников.
        /// </summary>
        /// <returns>Результат операции.</returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id.
        /// </summary>
        /// <returns>Результат операции.</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника.
        /// </summary>
        /// <param name="employee">Сотрудник.</param>
        /// <returns>Результат операции.</returns>
        [HttpPost]
        public async Task<ActionResult> CreateEmployeeAsync(EmployeeShortRequest employee)
        {
            if (employee == null)
            {
                return BadRequest("Employee can not be null.");
            }

            await _employeeRepository.CreateAsync(MapEmployee(employee));
            return Ok();
        }

        /// <summary>
        /// Обновить сотрудника.
        /// </summary>
        /// <param name="employee">Сотрудник.</param>
        /// <returns>Результат операции.</returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeShortRequest employee)
        {
            var item = await _employeeRepository.GetByIdAsync(employee.Id);

            if (item == null)
            {
                return NotFound();
            }

            await _employeeRepository.UpdateAsync(MapEmployee(employee, item));
            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        /// <param name="id">Идентификатор сотрудника.</param>
        /// <returns>Результат операции.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            await _employeeRepository.DeleteAsync(id);
            return Ok(id);
        }

        private Employee MapEmployee(EmployeeShortRequest request, Employee employee = null)
        {
            return new Employee()
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                AppliedPromocodesCount = employee == null ? 0 : employee.AppliedPromocodesCount,
                Roles = employee == null ? new List<Role>() : employee.Roles
            };
        }
    }
}